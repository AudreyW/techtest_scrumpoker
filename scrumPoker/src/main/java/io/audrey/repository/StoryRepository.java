package io.audrey.repository;

import io.audrey.models.Story;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Scope("singleton")
public class StoryRepository {

    public List<Story> allStory = new ArrayList<>();

    // Seed Data
    public StoryRepository(){
        HashMap<String, String> p = new HashMap<String, String>();
        p.put("Audrey","1");
        Story story1 = new Story( 1,"Implement chat feature",  "Story_01" , p);
        allStory.add(story1);
        p.put("Audrey","8");
        Story story2 = new Story( 2,"Add KYC UI to back office",  "Story_02", p);
        allStory.add(story2);
        p.put("Audrey","13");
        Story story3 = new Story( 3,"Create FX market stop order",  "Story_03", p);
        allStory.add(story3);
    }

    public List<Story> getStories() {
    return allStory;
    }


    public int  updateStory(int id,  String userName, String userStoryPoint ){
        Optional<Story> found = this.allStory.stream().filter(m -> m.getId() == id).findFirst();
        if(found.isPresent()){
            Story updatingStory = found.get();
            updatingStory.getResults().put(userName, userStoryPoint);
        }
        return id;
    }



}
