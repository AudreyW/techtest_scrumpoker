package io.audrey.models;

import java.util.HashMap;

public class Story {
    private int id;
    private String name;
    private String content;
    private HashMap <String, String> results;

    public Story( Integer id, String content, String name, HashMap<String, String> results) {
        this.id = id;
        this.name = name;
        this.content = content;
        this.results = results;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public HashMap<String, String> getResults() {
        return results;
    }

    public void setResults(HashMap<String, String> results) {
        this.results = results;
    }

}
