package io.audrey.controllers;

import io.audrey.forms.StoryPointSubmitForm;
import io.audrey.models.Story;
import io.audrey.repository.StoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class StoryController {

    @Autowired
    StoryRepository storyRepository;

    // For get story and story status
    @RequestMapping(value = "/stories", method = RequestMethod.GET)
    public List<Story> getStories(){
        return this.storyRepository.getStories();
    }

    // For clear story points
    @RequestMapping(value="/point", method = RequestMethod.POST)
    public int updateStory(@RequestBody StoryPointSubmitForm storyPointSubmitForm) {
        return this.storyRepository.updateStory(storyPointSubmitForm.getId(),storyPointSubmitForm.getUserName(),storyPointSubmitForm.getUserStoryPoint());
    }

}


