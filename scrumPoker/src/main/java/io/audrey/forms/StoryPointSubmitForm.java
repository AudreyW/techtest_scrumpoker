package io.audrey.forms;

public class StoryPointSubmitForm {
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private Integer id;
    private String userName;
    private String userStoryPoint;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserStoryPoint() {
        return userStoryPoint;
    }

    public void setUserStoryPoint(String userStoryPoint) {
        this.userStoryPoint = userStoryPoint;
    }


}
