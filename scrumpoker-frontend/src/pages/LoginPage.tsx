import React, { useState } from "react"
import { useDispatch } from "react-redux"
import { login } from "../redux/auth/action"
import './loginPage.scss'
import imgMeeting from '../assets/img_meeting.png';

export function LoginPage() {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const dispatch = useDispatch();

    return (
        <div className="loginPage">
            <form className="loginForm" onSubmit={event => {
                event.preventDefault();
                dispatch(login(username, password))
            }}>
                <input className="inputItem" type="text"
                    value={username}
                    onChange={event => setUsername(event.currentTarget.value)}
                    placeholder="Email Address"
                />
                <input className="inputItem" type="password"
                    value={password}
                    onChange={event => setPassword(event.currentTarget.value)}
                    placeholder="Password"
                />
                <input className="inputItem" type="submit" />
            </form>
            <img src={imgMeeting} alt="meeting-icon" className="imgMeeting" />
        </div>
    )
}
