import React, { useEffect } from 'react';
import { RootState } from '../store';
import { useDispatch, useSelector } from 'react-redux';
import StoryRow from '../components/story/StoryRow';
import { fetchStories } from '../redux/story/actions';
import { Link } from 'react-router-dom';

export default function StoryPage() {
    const stories = useSelector((state: RootState) => state.story.storyData)
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated)

    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(fetchStories());
    }, [dispatch]);

    return (
        <>
            { isAuthenticated ?
                stories.map(story => (
                    <StoryRow story={story} />
                ))
                : <Link to="/">Please Login</Link>
            }
        </>
    )
}