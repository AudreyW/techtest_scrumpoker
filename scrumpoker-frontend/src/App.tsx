import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import Header from './components/header/Header';
import { LoginPage } from './pages/LoginPage';
import StoryPage from './pages/StoryPage';
import { fetchStories } from './redux/story/actions';
import { RootState } from './store';



function App() {

  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated)
  const dispatch = useDispatch()

  useEffect(() => {
    if (isAuthenticated) {
      dispatch(fetchStories())
    }
  },[dispatch,isAuthenticated]);

  return (
    <div className="App">
      <Header />
      <div className="main">
        <Switch>
          <Route path="/" exact><LoginPage /></Route>
          <Route path="/story" exact><StoryPage /></Route>
          <Route path="/story/:id" exact><StoryPage /></Route>
        </Switch>
      </div>
      <footer>
        &copy; Audrey Wong
      </footer>
    </div>
  );
}

export default App;
