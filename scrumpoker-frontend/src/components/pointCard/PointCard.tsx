import React from 'react';
import './pointCard.scss';
import { Card, Button, CardTitle, Row, Col } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { submitStoryPointById } from '../../redux/story/actions';
import useReactRouter from 'use-react-router';
import { RootState } from '../../store';
import Swal from 'sweetalert2';


export default function PointCard() {
  const { match: { params: { id: story_id } } } = useReactRouter<{ id: string }>();
  const storyId: number = + story_id
  const userName = useSelector((state: RootState) => state.auth.username)
  const dispatch = useDispatch();

  // For checking the userName status
  if (userName == null) {
    Swal.fire({
      icon: 'info',
      title: `Please Login`,
      showCloseButton: true,
      showConfirmButton: false,
      footer: 'Scrum Poker'
    })
    return <div>Please Login</div>
  } else { 
    
  }

  return (
    <div className="cardField">
      <Row>
        <Col sm="3">
          <Card body>
            <CardTitle tag="h5">1</CardTitle>
            <Button onClick={() => {
              dispatch(submitStoryPointById(
                storyId ,
                userName!,
                '1'
              ))
            }}>Submit</Button>
          </Card>
        </Col>
        <Col sm="3">
          <Card body>
            <CardTitle tag="h5">2</CardTitle>
            <Button onClick={() => {
              dispatch(submitStoryPointById(
                storyId ,
                userName!,
                '2'
              ))
            }}>Submit</Button>
          </Card>
        </Col>
        <Col sm="3">
          <Card body>
            <CardTitle tag="h5">3</CardTitle>
            <Button onClick={() => {
              dispatch(submitStoryPointById(
                storyId ,
                userName!,
                '3'
              ))
            }}>Submit</Button>
          </Card>
        </Col>
        <Col sm="3">
          <Card body>
            <CardTitle tag="h5">5</CardTitle>
            <Button onClick={() => {
              dispatch(submitStoryPointById(
                storyId ,
                userName!,
                '5'
              ))
            }}>Submit</Button>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col sm="3">
          <Card body>
            <CardTitle tag="h5">8</CardTitle>
            <Button onClick={() => {
              dispatch(submitStoryPointById(
                storyId ,
                userName!,
                '8'
              ))
            }}>Submit</Button>
          </Card>
        </Col>
        <Col sm="3">
          <Card body>
            <CardTitle tag="h5">13</CardTitle>
            <Button onClick={() => {
              dispatch(submitStoryPointById(
                storyId ,
                userName!,
                '13'
              ))
            }}>Submit</Button>
          </Card>
        </Col>
        <Col sm="3">
          <Card body>
            <CardTitle tag="h5">No_idea</CardTitle>
            <Button onClick={() => {
              dispatch(submitStoryPointById(
                storyId ,
                userName!,
                'No_idea'
              ))
            }}>Submit</Button>
          </Card>
        </Col>
        <Col sm="3">
          <Card body>
            <CardTitle tag="h5">Resign</CardTitle>
            <Button onClick={() => {
              dispatch(submitStoryPointById(
                storyId ,
                userName!,
                'Resign'
              ))
            }}>Submit</Button>
          </Card>
        </Col>
      </Row>
    </div>
  )
}