import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Collapse, Nav, Navbar, NavbarBrand, NavItem, NavLink } from 'reactstrap';
import { logOut } from '../../redux/auth/action';
import { RootState } from '../../store';
import './header.scss';

export default function Header() {

  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated)
  const userName = useSelector((state: RootState) => state.auth.username)
  const dispatch = useDispatch()

  return (
    <div className="navBar">
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/"><i className="fab fa-think-peaks"></i> Scrum Poker</NavbarBrand>
        <Collapse navbar>
          <Nav className="ml-auto" navbar>
            {isAuthenticated &&
            <>
          <NavLink>
            <text>Hi, {userName}</text>
          </NavLink>
              <NavItem>
                <NavLink className="nav-link" onClick={() => {
                  logOut(dispatch)
                }}>Logout <i className="far fa-hand-point-right"></i></NavLink>
              </NavItem>
              </>
            }
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  )
}