import React from 'react';
import PointCard from '../pointCard/PointCard';
import Results from '../results/Results';
import './story.scss';

export default function StoryDetail() {
    return (
        <div className="storyContent">
            <h6 className="subtitle"> <i className="fas fa-user-edit"></i> Submit estimate</h6>
            <PointCard />
            <h6 className="subtitle"><i className="fas fa-poll-h"></i> Results</h6>
            <Results />
        </div>
    )
}
