import React from 'react';
import { Story } from '../../redux/story/reducer';
import './storyRow.scss';
import { Card, CardText, CardFooter } from 'reactstrap';
import { useDispatch } from 'react-redux';
import { useRouteMatch } from "react-router-dom"
import { replace } from 'connected-react-router';
import StoryDetail from './StoryDetail';


export default function StoryRow(props: { story: Story }) {
    const story = props.story
    const routeMatch = useRouteMatch<{ id?: string }>()
    const isOpen = parseInt(routeMatch.params.id || '') === story.id
    const dispatch = useDispatch()

    return (
        <div>
            <div className={"storyRow" + (isOpen ? " open" : "")} onClick={() => { dispatch(replace('/story/' + story.id)) }}>
                <Card body className="storyRowItem">
                    <CardText><i className="fas fa-chalkboard"></i> {story.content}</CardText>
                    <CardFooter className="storyRowFooter">| {story.name} | </CardFooter>
                </Card>
            </div>
            { isOpen && <StoryDetail />}
        </div>
    )
}