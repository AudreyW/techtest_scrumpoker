import React from 'react';
import { useSelector } from 'react-redux';
import { Table } from 'reactstrap';
import useReactRouter from 'use-react-router';
import { RootState } from '../../store';
import './results.scss';

export default function Results() {
    const { match: { params: { id: story_id } } } = useReactRouter<{ id: string }>();
    const story_Data = useSelector((state: RootState) => state.story.storyData)

    // Get particular story's results
    const storyId: number = + story_id
    console.log(story_id)
    console.log(story_Data)
    const results = story_Data.find((story)=> story.id === storyId)!.result

    const story_results = results.map((result, index) => (
        <tr key={index}>
            <th scope="row">{index + 1}</th>
            <td>{result.userName}</td>
            <td>{result.userStoryPoint}</td>
        </tr>
    ))

    return (

        <div className="resultTable">
            { (results && results.length) ?
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Story Points</th>
                        </tr>
                    </thead>
                    <tbody>
                        {story_results}
                    </tbody>
                </Table> :
                'No result yet, please submit your story point.'
            }
        </div>
    )
}