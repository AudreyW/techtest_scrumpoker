import { Dispatch } from "react"
import { RootState } from "../../store"
import { Story } from "./reducer"
import base64 from "base-64"

export function fetchStories() {
    return async (dispatch: Dispatch<any>, getState:() => RootState) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/stories`, {
            })
            const results = await res.json()
            const newResults = results.map((result: any) => ({
                id: result.id,
                name: result.name,
                content: result.content,
                result: Object.keys(result.results).map((key) => ({
                    userName: key,
                    userStoryPoint: result.results[key],
                })),
            }));
            dispatch(loadStories(newResults));
        } catch (err) {
            console.log(err.message)
            
        }
    }
}

export function addNewStory(
    name: string,
    content: string,
) {
    return async (dispatch: Dispatch<any>) => {
        try {
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/stories`,{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: name,
                    content: content
                })
            })
            const json = await res.json();
            if ( json.success === true) {
                dispatch(fetchStories());
            } else {
                dispatch(fetchStories()); // TODO handle error
            }
        } catch(err) {
            console.log(err)
        }
    }
}

export function submitStoryPointById(
    id: number,
    userName: string, 
    userStoryPoint: string,
) {
    return async (dispatch: Dispatch<any>, getState:() => RootState) => {
        try {
            const {username,password} = getState().auth
            console.log(getState())
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/point`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Basic ' + base64.encode(username + ":" + password,)
                },
                body: JSON.stringify({
                    id: id,
                    userName: userName,
                    userStoryPoint: userStoryPoint
                })
            })
            const json = await res.json();
            if ( json.success === true) {
                dispatch(fetchStories());
            } else {
                dispatch(fetchStories()); // TODO handle error
            }
        } catch(err) {
            console.log(err)
        }
    }

}

function loadStories(storyData: Story[]) {
    return {
        type: "@@full_story/LOAD_STORY_DETAIL" as "@@full_story/LOAD_STORY_DETAIL",
        storyData: storyData
    }
}

function failed(type: "SERVER_CONNECTION_ERROR" | "SERVER_ERROR_RESPONSE", message: string) {
    return {
        type: type
    }
}


export type fullStoryActions = ReturnType<typeof loadStories>
export type fullStoryFailure = ReturnType<typeof failed>
