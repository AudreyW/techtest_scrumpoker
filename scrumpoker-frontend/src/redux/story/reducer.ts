import { fullStoryActions, fullStoryFailure } from "./actions";

export interface Story {
    id: number,
    name: string,
    content: string,
    result: {
        userName: string,
        userStoryPoint: string,
    }[]
}

export interface StoryState {
    storyData: Story[]
}

const initialState: StoryState = {
    storyData: [ ]
}

export const storyReducer = (state: StoryState = initialState, action: fullStoryActions | fullStoryFailure): StoryState => {
    switch(action.type) {
        case "@@full_story/LOAD_STORY_DETAIL":
            return {
                ...state,
                storyData: action.storyData
            } 
            default:
                return state;
    } 
}