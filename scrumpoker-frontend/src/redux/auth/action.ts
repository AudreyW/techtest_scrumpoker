import { push } from "connected-react-router"
import { Dispatch } from "redux"
import base64 from "base-64"


export function loginSuccess(username: string, password: string) {
  return {
    type: '@@auth/LOGIN_SUCCESS' as '@@auth/LOGIN_SUCCESS',
    username,
    password
  }
}

export function loginFailed() {
  return {
    type: '@@auth/LOGIN_FAILED' as '@@auth/LOGIN_FAILED'
  }
}

export function logoutSuccess() {
  return {
    type: '@@auth/LOGOUT_SUCCESS' as '@@auth/LOGOUT_SUCCESS'
  }
}

export type AuthActions = ReturnType<typeof loginSuccess> |
                          ReturnType<typeof loginFailed> |
                          ReturnType<typeof logoutSuccess> 

export function login(username: string, password: string) {
  return async (dispatch: Dispatch<any>) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + base64.encode(username + ":" + password,)
      },
      body: JSON.stringify({ username, password })
    })
    const json = await res.json();
    if (json) {
      localStorage.setItem('username', username)
      localStorage.setItem('password', password)
      dispatch(loginSuccess(username,password))
      dispatch(push("/story"))
    } else {
      dispatch(loginFailed())
    }
  }
}

export function logOut(dispatch: Dispatch<any>) {
  localStorage.removeItem('username')
  localStorage.removeItem('password')
  dispatch(logoutSuccess())
  dispatch(push("/"))
}

