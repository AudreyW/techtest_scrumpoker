import { AuthActions } from "./action"

export interface AuthState {
  isAuthenticated: boolean | null
  username: string | null
  password: string | null
}

const initialState: AuthState = {
  isAuthenticated: null,
  username: localStorage.getItem('username'),
  password: localStorage.getItem('password'),
}

export const authReducer = (state: AuthState = initialState, action: AuthActions): AuthState => {
  if (action.type === '@@auth/LOGIN_SUCCESS') {
    return {
      ...state,
      isAuthenticated: true,
      username: action.username,
      password: action.password,
    }
  } else if (action.type === '@@auth/LOGOUT_SUCCESS') {
    return {
      ...state,
      isAuthenticated: false,
      username: null,
      password: null,
    }
  }

  return state;
}
