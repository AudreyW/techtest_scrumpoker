import { connectRouter, routerMiddleware, RouterState } from "connected-react-router";
import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { createBrowserHistory } from "history";
import { storyReducer, StoryState } from "./redux/story/reducer";
import { authReducer, AuthState } from "./redux/auth/reducer";
import thunk from 'redux-thunk';

export const history = createBrowserHistory()

export interface RootState {
    router: RouterState
    story: StoryState
    auth: AuthState
}

export const rootReducer = combineReducers ({
    router: connectRouter(history),
    story: storyReducer,
    auth: authReducer
})

declare global{
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__:any
    }
  }
  
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  

export const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history)))
)
    